-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 20 Sty 2024, 23:54
-- Wersja serwera: 10.4.27-MariaDB
-- Wersja PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `flask`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kwiaty`
--

CREATE TABLE `kwiaty` (
  `nazwa` varchar(35) NOT NULL,
  `opis` varchar(10000) NOT NULL,
  `obraz` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Zrzut danych tabeli `kwiaty`
--

INSERT INTO `kwiaty` (`nazwa`, `opis`, `obraz`) VALUES
('Figowiec Benjamina', 'Figowiec Benjamina, znany również jako Ficus benjamina, to popularna roślina doniczkowa o eleganckim wyglądzie. Pochodzi z obszarów Azji Południowej i jest ceniony za swoje błyszczące, skórzaste liście oraz zwieszające się gałązki. Figowiec Benjamina jest często uprawiany jako roślina ozdobna wewnątrz pomieszczeń. Wymaga jasnego światła, ale unika bezpośredniego nasłonecznienia, oraz regularnego podlewania.', 'images/kwiaty/FigowiecBenjamina.jpg'),
('Fiołek afrykański', 'Fiołek afrykański, znany także jako Saintpaulia, to mała roślina doniczkowa o uroczych kwiatach i kształcie liści. Pochodzi z wilgotnych lasów tropikalnych Afryki. Charakteryzuje się delikatnymi, jedwabistymi kwiatuszkami w różnych kolorach, takich jak fioletowy, biały, różowy czy niebieski. Jest to roślina łatwa w pielęgnacji, wymagająca umiarkowanego podlewania i jasnego, ale rozproszonego światła.', 'images/kwiaty/FiolekAfrykanski.jpg'),
('Kalanchoe kwitnące', 'Kalanchoe kwitnące to roślina doniczkowa znana ze swoich efektownych, kolorowych kwiatów. Pochodzi z obszarów tropikalnych i subtropikalnych, a jej charakterystyczną cechą są gęste kłosy kwiatowe w odcieniach różu, czerwieni, pomarańczy czy żółci. Roślina ta jest stosunkowo łatwa w pielęgnacji, wymaga umiarkowanego podlewania i jasnego światła.', 'images/kwiaty/KalanchoeKwitnace.jpg'),
('Monstera', 'Monstera domowa, znana również jako Roślina Serowa, to popularna roślina doniczkowa, zdobywająca coraz większą popularność wśród miłośników zieleni w domu. Jej wyjątkowe liście, charakteryzujące się głębokimi wycięciami i dziurami, nadają jej niepowtarzalny, tropikalny wygląd, który przyciąga uwagę.\n\n    Główne cechy monstery to jej jasnozielone, sercowate liście, które rozwijają się w charakterystyczne otwory, przypominające dziury w serze. Ta unikalna struktura liści sprawia, że roślina prezentuje się nie tylko estetycznie, ale także dodaje przestrzeni wnętrza odrobinę egzotycznego uroku.\n\n    Monstera jest uważana za łatwą w pielęgnacji roślinę, co czyni ją idealną dla osób, które dopiero zaczynają swoją przygodę z uprawą roślin w domu. Wymaga umiarkowanego podlewania i umieszczenia w miejscu o jasnym, rozproszonym świetle. Roślina ta również dobrze znosi warunki domowe, co sprawia, że jest popularnym wyborem do ozdabiania różnych pomieszczeń.\n\n    Jednak warto zauważyć, że monstera jest rośliną trującą dla zwierząt, dlatego należy zachować ostrożność, jeśli w domu są domowe pupile. Pomimo tego aspektu, monstera domowa pozostaje atrakcyjnym dodatkiem do wnętrza, dodając świeżość i tropikalny urok do każdego pomieszczenia.', 'images/kwiaty/Monstera.jpg'),
('Pieniążek', 'Pilea peperomioides, znana także jako \"Roślina Pieniążek\", to gatunek rośliny z rodziny okrężnicowatych. Pochodzi z południowych Chin i cechuje się okrągłymi, mięsistymi liśćmi o kształcie przypominającym monety. Liście są jasnozielone i umieszczone na długich ogonkach, nadając roślinie elegancki wygląd. Pilea peperomioides osiąga zazwyczaj wysokość od 20 do 30 cm i jest popularna jako roślina doniczkowa.', 'images/kwiaty/Pieniazek.jpg'),
('Storczyk', 'Storczyki to popularne rośliny kwiatowe z rodziny storczykowatych (Orchidaceae). Charakteryzują się pięknymi, kolorowymi kwiatami o różnych kształtach i rozmiarach. Są one uznawane za eleganckie rośliny doniczkowe, ale wymagają specjalnej opieki. Storczyki preferują jasne, pośrednie światło i dobrze przepuszczalny podłoże. Ważne jest również dostarczanie im odpowiedniej wilgotności. Są dostępne w wielu odmianach, oferując różnorodność kolorów i wzorów.', 'images/kwiaty/Storczyk.jpg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `typkonta`
--

CREATE TABLE `typkonta` (
  `typ_ID` int(11) NOT NULL,
  `nazwa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Zrzut danych tabeli `typkonta`
--

INSERT INTO `typkonta` (`typ_ID`, `nazwa`) VALUES
(1, 'user'),
(2, 'mod'),
(3, 'admin');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `username` varchar(35) NOT NULL,
  `password` varchar(350) NOT NULL,
  `imie` varchar(35) DEFAULT NULL,
  `nazwisko` varchar(35) DEFAULT NULL,
  `email` varchar(35) DEFAULT NULL,
  `telefon` int(10) DEFAULT NULL,
  `typKonta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`username`, `password`, `imie`, `nazwisko`, `email`, `telefon`, `typKonta`) VALUES
('1', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL, NULL, NULL, NULL, 1),
('Ireneusz', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'Ireneusz', 'Kwiat', 'Irek10@wp.pl', 123456789, 1),
('Karolina', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'Karolina', 'Żonkil', 'Kwiayek61@onet.pl', 987654321, 1),
('test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'Arkadiusz', 'Kowalski', 'testttttt', 123456789, 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kwiaty`
--
ALTER TABLE `kwiaty`
  ADD PRIMARY KEY (`nazwa`);

--
-- Indeksy dla tabeli `typkonta`
--
ALTER TABLE `typkonta`
  ADD PRIMARY KEY (`typ_ID`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`),
  ADD KEY `typKonta` (`typKonta`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `typkonta`
--
ALTER TABLE `typkonta`
  MODIFY `typ_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`typKonta`) REFERENCES `typkonta` (`typ_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
