from flask import Flask, render_template, request, redirect, url_for, session, Response
from flask_mysqldb import MySQL
from datetime import datetime
import os
import hashlib

app = Flask(__name__)
app.secret_key = 'klucz'    # Klucz aplikacji, do zabezpieczenia sesji użytkowników
app.config['UPLOAD_FOLDER'] = 'static/images/kwiaty'  # Folder do przechowywania przesłanych plików
app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg'}   # Deklaracja dozwolonych formatów plików

# Konfiguracja MySQL
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'flask'

mysql = MySQL(app)

# Funkcja sprawdzająca, czy rozszerzenie pliku jest dozwolone
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

# Endpoint dla strony głównej
@app.route('/')
def home():
    return render_template('home.html')

# Endpoint obsługujący stronę katalogu
@app.route('/katalog', methods = ['POST', 'GET'])
def katalog():
    cursor = None
    try:
        conn = mysql.connect

        cursor = conn.cursor()
        cursor.execute("SELECT * FROM kwiaty")
        data = cursor.fetchall()
        # Przekazanie danych do szablonu
        kwiaty = [
                {'nazwa': row[0], 'opis': row[1], 'obraz': row[2]}
                for row in data
            ]
        return render_template('katalog.html', data=data, kwiaty=kwiaty)

    except Exception:
        error = "Nie można połączyć się z bazą danych"
        return render_template('komunikat.html', error=error)
    finally:
        if cursor:
            cursor.close()

# Endpoint obsługujący szczegóły kwiatka o określonej nazwie
@app.route('/kwiat/<nazwa>')
def kwiat_detail(nazwa):
    cursor = None
    try:
        conn = mysql.connect
        if conn is not None:
            cursor = conn.cursor()
        
        # Pobranie szczegółów o określonej nazwie z bazy danych
        cursor.execute("SELECT * FROM kwiaty WHERE nazwa=%s", (nazwa,))
        kwiat = cursor.fetchone()

        kwiat_info = {'nazwa': kwiat[0], 'opis': kwiat[1], 'obraz': kwiat[2]}
        return render_template('kwiat_detail.html', kwiat=kwiat_info)
        
    except Exception:
        error = "Nie można połączyć się z bazą danych"
        return render_template('komunikat.html', error=error)
    finally:
        if cursor:
            cursor.close()

# Endpoint obsługujący dodawanie nowego kwiatka
@app.route('/dodaj_kwiat', methods=['GET', 'POST'])
def dodaj_kwiat():
    if 'username' in session:
        username = session['username']
        if request.method == 'POST':
            nazwa = request.form['nazwa']
            opis = request.form['opis']

            file = request.files['obraz']

            # Sprawdzenie czy plik jest dozwolonego formatu
            if file and allowed_file(file.filename):
                obraz_extension = file.filename.rsplit(".", 1)[1]
                obraz_link = f'images/kwiaty/{nazwa}.{obraz_extension}'

                cursor = None
                try:
                    conn = mysql.connect
                    if conn is not None:
                        cursor = conn.cursor()

                        # Sprawdzenie czy kwiatek o podanej nazwie już istnieje
                        cursor.execute("SELECT * FROM kwiaty WHERE nazwa=%s", (nazwa,))
                        existing_kwiat = cursor.fetchone()

                        if existing_kwiat:
                            error_flowerExisting = 'Kwiatek o podanej nazwie już istnieje'
                            return render_template('dodaj_kwiat.html', error=error_flowerExisting)

                        # Zapisanie informacji o kwiatku do bazy danych
                        cursor.execute("INSERT INTO kwiaty (nazwa, opis, obraz) VALUES (%s, %s, %s)", (nazwa, opis, obraz_link))
                        conn.commit()

                        # Zapisanie obrazu
                        filename = os.path.join(app.config['UPLOAD_FOLDER'], f'{nazwa}.{obraz_extension}')
                        file.save(filename)

                        succes_flowerAdded = 'Kwiat został dodany pomyślnie'
                        return render_template('dodaj_kwiat.html', succes=succes_flowerAdded)

                except Exception:
                    error = "Nie można połączyć się z bazą danych"
                    return render_template('komunikat.html', error=error)

                finally:
                    if cursor:
                        cursor.close()

                        # Zresetuj pola formularza
                        session['nazwa'] = ''
                        session['opis'] = ''

        return render_template('dodaj_kwiat.html')
    
    # Komunikat o braku dostępu, gdy użytkownik nie jest zalogowany
    error = 'Brak dostępu. Zaloguj się!'
    return render_template('komunikat.html', error=error)

# Endpoint obsługujący stronę z danymi kontaktowymi
@app.route('/kontakt')
def kontakt():
    return render_template('kontakt.html')

# Endpoint obsługujący wyświetlanie danych konta użytkownika
@app.route('/konto', methods=['GET'])
def konto():
    if 'username' in session:
        username = session['username']
        cursor = None
        try:
            conn = mysql.connect
            if conn is not None:
                cursor = conn.cursor()

                # Pobranie danych użytkownika z bazy danych
                cursor.execute("SELECT u.username, u.imie, u.nazwisko, u.email, u.telefon, t.nazwa AS typKonta FROM user u JOIN typKonta t ON u.typKonta = t.typ_ID WHERE u.username=%s", (username,))
                user_data = cursor.fetchone()

                user = {
                    'username': user_data[0],
                    'imie': user_data[1],
                    'nazwisko': user_data[2],
                    'email': user_data[3],
                    'telefon': user_data[4],
                    'typKonta': user_data[5]
                }

                return render_template('konto.html', user=user)

        except Exception:
            error = "Nie można połączyć się z bazą danych"
            return render_template('komunikat.html', error=error)
        
        finally:
            if cursor:
                cursor.close()

    # Jeżeli użytkownik nie jest zalogowany nastąpi przekierowanie na stronę rejestracji
    else:
       return render_template('rejestracja.html')
    
# Endpoint obsługujący aktualizację danych użytkownika
@app.route('/aktualizuj_dane', methods=['POST'])
def aktualizuj_dane():
    if 'username' in session:
        username = session['username']
        imie = request.form['imie']
        nazwisko = request.form['nazwisko']
        email = request.form['email']
        telefon = request.form['telefon']

        cursor = None
        try:
            conn = mysql.connect
            if conn is not None:
                cursor = conn.cursor()

                # Aktualizacja danych użytkownika w bazie danych
                cursor.execute("UPDATE user SET imie=%s, nazwisko=%s, email=%s, telefon=%s WHERE username=%s", (imie, nazwisko, email, telefon, username))
                conn.commit()

                # Pobranie zaktualizowanych danych
                cursor.execute("SELECT u.username, u.imie, u.nazwisko, u.email, u.telefon, t.nazwa AS typKonta FROM user u JOIN typKonta t ON u.typKonta = t.typ_ID WHERE u.username=%s", (username,))
                user_data = cursor.fetchone()

                user = {
                    'username': user_data[0],
                    'imie': user_data[1],
                    'nazwisko': user_data[2],
                    'email': user_data[3],
                    'telefon': user_data[4],
                    'typKonta': user_data[5]
                }

                # Walidacja imienia
                if not imie.isalpha():
                    error = 'Nieprawidłowe imię. Dozwolone są tylko litery'
                    return render_template('konto.html', error=error, user=user)

                # Walidacja nazwiska
                if not nazwisko.replace('-', '').isalpha():
                    error = 'Nieprawidłowe nazwisko. Dozwolone są tylko litery i myślnik'
                    return render_template('konto.html', error=error, user=user)

                # Walidacja adresu e-mail
                if '@' not in email or '.' not in email.split('@')[-1]:
                    error = 'Nieprawidłowy adres e-mail'
                    return render_template('konto.html', error=error, user=user)

                # Walidacja numeru telefonu
                if not telefon.isdigit():
                    error = 'Numer telefonu powinien zawierać tylko cyfry'
                    return render_template('konto.html', error=error, user=user)

                succes = 'Dane zaktualizowane!'
                return render_template('komunikat.html', succes=succes, user=user)

        except Exception:
            error = "Nie można połączyć się z bazą danych"
            return render_template('komunikat.html', error=error)
        
        finally:
            if cursor:
                cursor.close()
    
# Endpoint obsługujący zmianę hasła użytkownika
@app.route('/haslo', methods=['GET', 'POST'])
def haslo():
    if 'username' in session:
        username = session['username']
        error = None

        # Obsługa żądania POST do zmiany hasła
        if request.method == 'POST':
            old_password = request.form['old_password']
            new_password = request.form['new_password']
            confirm_password = request.form['confirm_password']

            # Sprawdź poprawność starego hasła
            cursor = None
            try:
                conn = mysql.connect
                cursor = conn.cursor()

                # Pobranie zahashowanego hasła z bazy danych dla danego użytkownika
                cursor.execute("SELECT * FROM user WHERE username=%s", (username,))
                user = cursor.fetchone()

                if user:
                    hashed_password_from_db = user[1]

                    # Porównanie hasła wprowadzonego przez użytkownika z zahashowanym hasłem z bazy danych
                    if hashlib.sha256(old_password.encode('utf-8')).hexdigest() != hashed_password_from_db:
                        error = 'Stare hasło jest nieprawidłowe'

                    # Porównanie nowego hasła wprowadzonego przez użytkownika z zahashowanym hasłem z bazy danych
                    elif hashlib.sha256(old_password.encode('utf-8')).hexdigest() == hashlib.sha256(new_password.encode('utf-8')).hexdigest():
                        error = 'Hasło jest obecnie używane'

                    # Porównanie nowego hasła z hasłem potwierdzającym
                    elif hashlib.sha256(new_password.encode('utf-8')).hexdigest() != hashlib.sha256(confirm_password.encode('utf-8')).hexdigest():
                        error = 'Nowe hasła nie pasują do siebie'
                    else:
                        # Zaktualizowanie hasła w bazie danych po zahashowaniu
                        hashed_new_password = hashlib.sha256(new_password.encode('utf-8')).hexdigest()
                        cursor.execute("UPDATE user SET password=%s WHERE username=%s", (hashed_new_password, username))
                        conn.commit()
                        succes = 'Hasło zmienione!'
                        return render_template('komunikat.html', succes=succes)


            except Exception:
                error = "Nie można połączyć się z bazą danych"
                return render_template('komunikat.html', error=error)

            finally:
                if cursor:
                    cursor.close()

        return render_template('haslo.html', error=error)

# Endpoint obsługujący rejestrację użytkownika
@app.route('/rejestracja', methods=['GET', 'POST'])
def rejestracja():
    error = None

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        confirm_password = request.form['confirm_password']

        cursor = None
        try:
            conn = mysql.connect
            cursor = conn.cursor()

            # Sprawdzenie czy użytkownik o podanej nazwie już istnieje
            cursor.execute("SELECT * FROM user WHERE username=%s", (username,))
            existing_user = cursor.fetchone()

            if existing_user:
                error = 'Użytkownik o tej nazwie już istnieje. Wybierz inną nazwę.'
            elif password != confirm_password:
                error = 'Nowe hasła nie pasują do siebie'
            else:
                # Haszowanie hasła przed zapisaniem do bazy danych
                hashed_password = hashlib.sha256(password.encode('utf-8')).hexdigest()
                # Dodanie nowego użytkownika do bazy danych
                cursor.execute("INSERT INTO user (username, password, typKonta) VALUES (%s, %s, %s)", (username, hashed_password, 1))
                conn.commit()
                session['username'] = username
                succes_userAdded = 'Użytkownik zarejestrowany!'
                return render_template('komunikat.html', succes=succes_userAdded)

        except Exception:
            error = "Nie można połączyć się z bazą danych"
            return render_template('komunikat.html', error=error)
        
        finally:
            if cursor:
                cursor.close()

    return render_template('rejestracja.html', error=error)

# Endpoint obsługujący logowanie użytkownika
@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        
        cursor = None
        try:
            conn = mysql.connect
            if conn is not None:
                cursor = conn.cursor()
                
                # Pobranie zahashowanego hasła z bazy danych dla danego użytkownika
                cursor.execute("SELECT * FROM user WHERE username=%s", (username,))
                user = cursor.fetchone()

                if user:
                    hashed_password_from_db = user[1]

                    # Porównanie hasła wprowadzonego przez użytkownika z zahashowanym hasłem z bazy danych
                    if hashlib.sha256(password.encode('utf-8')).hexdigest() == hashed_password_from_db:
                        session['username'] = username
                        succes = 'Użytkownik zalogowany'
                        return render_template('komunikat.html', succes=succes)
                    else:
                        error = 'Nieprawidłowe hasło'
                        return render_template('komunikat.html', error=error)
                else:
                    error = 'Nieprawidłowy login'
                    return render_template('komunikat.html', error=error)
        
        except Exception:
            error = "Nie można połączyć się z bazą danych"
            return render_template('komunikat.html', error=error)
        
        finally:
            if cursor:
                cursor.close()

    return redirect('/')

# Endpoint obsługujący wylogowywanie użytkownika
@app.route('/logout', methods=['POST'])
def logout():
    # Usuwanie informacji o zalogowanym użytkowniku z sesji
    session.pop('username', None)
    # Wyświetlenie komunikatu
    error = "Użytkownik wylogowany"
    return render_template('komunikat.html', error=error)

# Endpoint udostępniający czas serwera
@app.route("/czas-serwera")
def czas_serwera():
    def generuj_wiadomosci():
        while True:
            # Pobranie bieżącego czasu
            czas = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            yield "event: czas\ndata: {}\n\n".format(czas)

    return Response(generuj_wiadomosci(), mimetype="text/event-stream")

if __name__ == '__main__':
    app.run(debug=True)